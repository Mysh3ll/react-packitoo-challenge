export function saveFormBriefData(values) {
    return {
        type: 'BRIEF_FORM_SUBMITTED',
        values
    }
}

export function briefsHasErrored(bool) {
    return {
        type: 'BRIEFS_HAS_ERRORED',
        hasErrored: bool
    }
}

export function briefsHasSucceeded(bool) {
    return {
        type: 'BRIEFS_HAS_SUCCEEDED',
        hasSucceeded: bool
    }
}

export function briefsIsLoading(bool) {
    return {
        type: 'BRIEFS_IS_LOADING',
        isLoading: bool
    }
}

export function briefsFetchDataSuccess(briefs) {
    return {
        type: 'BRIEFS_FETCH_DATA_SUCCESS',
        briefs
    }
}

export function briefPostDataSuccess(brief) {
    return {
        type: 'BRIEF_POST_DATA_SUCCESS',
        brief
    }
}

export function briefsFetchData(url) {
    return (dispatch) => {
        dispatch(briefsIsLoading(true));
        fetch(url)
            .then((response) => {
                if (!response.ok) {
                    throw Error(response.statusText);
                }
                dispatch(briefsIsLoading(false));
                return response;
            })
            .then((response) => response.json())
            .then((briefs) => dispatch(briefsFetchDataSuccess(briefs)))
            .catch(() => dispatch(briefsHasErrored(true)));
    };
}

export function briefsPostData(url, values) {
    return (dispatch) => {
        dispatch(briefsHasSucceeded(false));
        fetch(url, {
            method: 'POST',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
            },
            body: JSON.stringify({
                title: values.title,
                comment: values.comment,
                productId: values.product,
            })
        })
            .then((response) => {
                if (!response.ok) {
                    throw Error(response.statusText);
                }
                dispatch(briefsHasSucceeded(true));
                return response;
            })
            .then((response) => response.json())
            .then((brief) => dispatch(briefPostDataSuccess(brief)))
            .catch(() => dispatch(briefsHasErrored(true)));
    };
}

