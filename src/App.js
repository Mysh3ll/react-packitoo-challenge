import React, {Component} from 'react';
import {connect} from 'react-redux';
import { BrowserRouter as Router, Route, Link } from "react-router-dom";
import {simpleAction} from './action-creators/simpleAction';
import './App.css';
import './components/BriefForm'
import BriefForm from "./components/BriefForm";
import BriefsList from "./components/BriefsList";

class App extends Component {

    simpleAction = (event) => {
        this.props.simpleAction();
    };

    render() {
        return (
            <Router>
                <div>
                    <ul>
                        <li>
                            <Link to="/">Form</Link>
                        </li>
                        <li>
                            <Link to="/briefs">Briefs</Link>
                        </li>
                    </ul>

                    <hr />

                    <Route exact path="/" component={BriefForm} />
                    <Route path="/briefs" component={BriefsList} />
                </div>
            </Router>
        );
    }
}

const mapStateToProps = state => ({
    ...state
});

const mapDispatchToProps = dispatch => ({
    simpleAction: () => dispatch(simpleAction())
});

export default connect(mapStateToProps, mapDispatchToProps)(App);
