import React, { Component} from 'react';
import { connect } from 'react-redux';
import { briefsFetchData } from '../action-creators/briefFormAction';

class BriefsList extends Component {
    componentDidMount() {
        this.props.fetchData('http://localhost:3000/briefs');
    }

    render() {
        if (this.props.productsHasErrored) {
            return <p>Sorry! there was an error loading the briefs</p>
        }

        if (this.props.briefsIsLoading) {
            return <p>Loading...</p>
        }

        return (
            <div>
                <ul>
                    {this.props.briefs.map(brief =>
                        <li key={brief.id} value={brief.id}>Title: {brief.title} | Comment: {brief.comment} | Product: {brief.productId}</li>
                    )}
                </ul>
            </div>

        );
    }
}

const mapStateToProps = (state) => ({
    briefs: state.briefs,
    briefsHasErrored: state.briefsHasErrored,
    briefsIsLoading: state.briefsIsLoading,
});

const mapDispatchToProps = (dispatch) => ({
    fetchData: (url) => dispatch(briefsFetchData(url)),
});

export default connect(mapStateToProps, mapDispatchToProps)(BriefsList);
