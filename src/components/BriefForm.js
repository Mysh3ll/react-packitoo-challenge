import React, { Component} from 'react';
import { connect } from 'react-redux';
import { productsFetchData } from '../action-creators/productsAction';
import { saveFormBriefData, briefsPostData } from '../action-creators/briefFormAction';
import Form from './Form';

class BriefForm extends Component {
    componentDidMount() {
        this.props.fetchData('http://localhost:3000/products');
    }

    handleSubmitForm = values => {
        this.props.saveFormBriefData(values);
        this.props.briefsPostData('http://localhost:3000/briefs', values);
    };

    render() {
        if (this.props.productsHasErrored) {
            return <p>Sorry! there was an error loading the products</p>
        }

        if (this.props.productsIsLoading || this.props.briefsIsLoading) {
            return <p>Loading...</p>
        }

        return (
            <div>
                <Form products={this.props.products} onSubmit={this.handleSubmitForm}/>
                {this.props.briefsHasErrored && <p>Sorry! there was an error posting the brief</p>}
                {this.props.briefsHasSucceeded && <p>Thanks! we will process your request</p>}
            </div>

        );
    }
}

const mapStateToProps = (state) => ({
    products: state.products,
    productsHasErrored: state.productsHasErrored,
    productsIsLoading: state.productsIsLoading,
    brief: state.brief,
    briefsHasErrored: state.briefsHasErrored,
    briefsIsLoading: state.briefsIsLoading,
    briefsHasSucceeded: state.briefsHasSucceeded,
});

const mapDispatchToProps = (dispatch) => ({
    fetchData: (url) => dispatch(productsFetchData(url)),
    saveFormBriefData: (values) => dispatch(saveFormBriefData(values)),
    briefsPostData: (url, values) => dispatch(briefsPostData(url, values))
});

export default connect(mapStateToProps, mapDispatchToProps)(BriefForm);
