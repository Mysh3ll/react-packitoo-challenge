import React from 'react';
import { reduxForm, Field, reset } from 'redux-form';

const validate = values => {
    const errors = {};
    if (!values.title) {
        errors.title = 'Required'
    }
    if (!values.comment) {
        errors.comment = 'Required'
    }
    if (!values.product) {
        errors.product = 'Required'
    }
    return errors;
};

const createRenderer = render => ({input, meta, label, ...rest}) =>
    <div>
        <label>{label}</label>
        {render(input, label, rest)}
        {meta.error && meta.touched && <span>{meta.error}</span>}
    </div>;

const RenderInput = createRenderer((input, label) =>
    <input {...input} placeholder={label}/>
);

const RenderSelect = createRenderer((input, label, {children}) =>
    <select {...input}>{children}</select>
);

let Form = ({handleSubmit, submitting, products}) =>
    <div>
        <form onSubmit={handleSubmit}>
            <Field name="title" label="Title" component={RenderInput}/>
            <Field name="comment" label="Comment" component={RenderInput}/>
            <Field name="product" label="Product" component={RenderSelect}>
                <option />
                {products.map(product => <option key={product.id} value={product.id}>{product.name}</option>)}
            </Field>
            <button type="submit" disabled={submitting}>Submit</button>
        </form>
    </div>;

const afterSubmit = (result, dispatch) =>
    dispatch(reset('packitoo'));

Form = reduxForm({
    form: 'packitoo',
    // destroyOnUnmount: false,
    validate,
    onSubmitSuccess: afterSubmit
})(Form);

export default Form;