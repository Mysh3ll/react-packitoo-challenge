export function briefFormSubmitted(state = [], action) {
    switch(action.type) {
        case 'BRIEF_FORM_SUBMITTED':
            return action.values;
        default:
            return state;
    }
}

export function briefsHasErrored(state = false, action) {
    switch(action.type) {
        case 'BRIEFS_HAS_ERRORED':
            return action.hasErrored;
        default:
            return state;
    }
}

export function briefsHasSucceeded(state = false, action) {
    switch(action.type) {
        case 'BRIEFS_HAS_SUCCEEDED':
            return action.hasSucceeded;
        default:
            return state;
    }
}

export function briefsIsLoading(state = false, action) {
    switch(action.type) {
        case 'BRIEFS_IS_LOADING':
            return action.isLoading;
        default:
            return state;
    }
}

export function briefs(state = [], action) {
    switch(action.type) {
        case 'BRIEFS_FETCH_DATA_SUCCESS':
            return action.briefs;
        default:
            return state;
    }
}

export function brief(state = [], action) {
    switch(action.type) {
        case 'BRIEF_POST_DATA_SUCCESS':
            return action.brief;
        default:
            return state;
    }
}
