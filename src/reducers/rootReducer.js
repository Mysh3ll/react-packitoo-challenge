import { combineReducers } from 'redux';
import simpleReducer from './simpleReducer';
import { products, productsHasErrored, productsIsLoading } from './productReducer';
import { briefFormSubmitted, briefs, brief, briefsHasErrored, briefsIsLoading, briefsHasSucceeded } from './briefFormReducer';
import { reducer as formReducer } from 'redux-form';

export default combineReducers({
    simpleReducer,
    products,
    productsHasErrored,
    productsIsLoading,
    briefFormSubmitted,
    briefs,
    brief,
    briefsHasErrored,
    briefsIsLoading,
    briefsHasSucceeded,
    form: formReducer
});